#!/usr/bin/python

import feedparser, sqlite3, datetime, hashlib, sys

# Load Feed URLs.
con = sqlite3.connect('data/data.db')
con.row_factory = sqlite3.Row
c = con.cursor()

# Set datime since "The default datetime adapter is deprecated as of Python 3.12". 
def adapt_datetime_iso(val):
    """Adapt datetime.datetime to timezone-naive ISO 8601 date."""
    return val.isoformat()

sqlite3.register_adapter(datetime.datetime, adapt_datetime_iso)

urls = [
  ("https://ics-cert.us-cert.gov/alerts/alerts.xml", "ICS-Cert Alerts", 1452515560294 ),
  ("https://ics-cert.us-cert.gov/advisories/advisories.xml", "ICS-Cert Advisories", 1452515560294 ),
  ("http://feeds.trendmicro.com/Anti-MalwareBlog?format=xml", "Trend Micro", 1452515560291 ),
  # ("http://www.darkreading.com/rss_simple.asp?f_n=644&f_ln=Attacks/Breaches, "Dark Reading Attack and Breaches", 1484487903482),
  ("http://krebsonsecurity.com/feed/", "Krebs on Security", 1452515560293 ),
  ("http://feeds.feedburner.com/darknethackers", "Dark Net Hackers", 1452515560293 ),
  ("https://ics.sans.org/blog/feed/", "SANS ICS", 1453303757830 ),
  ("https://labsblog.f-secure.com/feed/", "f-secure", 1453289027155 ),
  ("https://feeds.feedburner.com/TheHackersNews", "The Hackers News", 1474377882033 ),
  ("https://www.fireeye.com/blog/threat-research/_jcr_content.feed", "Fire Eye Threat Research", 1453305683590 ),
  ("http://www.darkreading.com/rss_simple.asp?f_n=659&f_ln=Threat%20Intelligence", "Dark Reading - Threat Intelligence", 1484487903484 ),
  #("https://www.reddit.com/r/netsec/new.rss", "Reddit NetSec", 1467281864184 ), # 2019-12-28 Commented out due to error | disabled in DB
  ("https://securelist.com/feed/", "Secure List", 1452515560293 ),
  ("http://cryptanalysis.eu/blog/feed/", "Cryptanalysis", 1452515560294 ),
  ("https://feeds.feedburner.com/eset/blog?format=xml", "ESET", 1453132418591 ),
  ("https://ics-cert.us-cert.gov/monitors/monitors.xml", "ICS-Cert Monitors", 1452515560295 ),
  ("https://www.schneier.com/blog/atom.xml", "Schneier on Security", 1452515560292 ),
  ("http://blog.invisiblethings.org/feed.xml", "Invisible Things Labs", 1452515560293 )
]

# 2019 Sep 04: Added el reg
urls = [
  ("https://www.theregister.co.uk/security/headlines.atom", "The Register", 1567616090 )
]

# 2019 Sept 09: Added Wired
urls = [("https://www.wired.com/feed/security/rss", "Wired", 1568046658 )]

# 2020 May 08: Added Citizen Lab
urls = [("https://citizenlab.ca/feed", "Citizen Lab", 1588933827 )]

# 2021 May 21: Added Microsoft Security Blog
urls = [("https://www.microsoft.com/security/blog/feed/", "Microsoft Security Blog", 1621602091 )]

# Initialise tables.
try:
	c.execute('''CREATE TABLE feed (FID integer primary key not null, feed_link text unique, feed_title text, date_added date, enable boolean DEFAULT 'True')''')
	c.execute('''CREATE TABLE news (NID string primary key not null, title text, author text, link text unique, description text, published date, date_added date, source_feed integer, gen boolean)''')
	con.commit()
except sqlite3.OperationalError:
	pass

# Add feed source
try:
	con.executemany('INSERT INTO feed (feed_link, feed_title, date_added) VALUES (?, ?, ?)', urls)
	con.commit()
except sqlite3.IntegrityError:
	pass

print("Parsing all feeds:")
newData = []
for row in c.execute('SELECT FID, feed_link FROM feed WHERE enable = "true" '):
	
	print("\t-", row['feed_link'], "...", end=' ')
	
	for e in feedparser.parse(row['feed_link']).entries:

		if not hasattr(e, 'author'):
			e.author = 'N/A'

		if not hasattr(e, 'description'):
				e.description = 'N/A'

		if not "link" in e:
			print("[not ok] skipping. Should have been:  ",  end=' ' )
			break

		# Look for a published timestamp, if none found use the current time.
		published = datetime.datetime.now()
		if 'published' in e:
			published = e.published
		elif 'updated' in e:
			published = e.updated
		
		newData.append([hashlib.md5(e.link.encode('ascii')).hexdigest(), e.title.encode('ascii', 'ignore').decode('ascii'), e.author.encode('ascii', 'ignore').decode('ascii'), e.link.encode('ascii', 'ignore').decode('ascii'), e.description.encode('ascii', 'ignore').decode('ascii'), str(published).encode('ascii', 'ignore').decode('ascii'), datetime.datetime.now(), row['FID'], False])

		# print type(newData[-1][1]), type(newData[-1][2])
	print("[ok] found", len(newData))

print("Inserting into database:")
for n in newData:
	print("\t-", n[3], end=' ')
	try:
		con.execute('INSERT INTO news VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)', n)
		con.commit()
	except sqlite3.IntegrityError as e:
		# This is ignored because we have already added this entry into the database (Based on the link).
		print("[skipping].")
	except Exception as e:
		print(e)
	else:
		print("[ok].")
