# Port 22 News Feed Backend

This repository contains two scripts: 
 
 - **Update.py**: Retrieves and stores (SQLite) all items from each feed.
 - **Extract.py**: Extracts news items from the local database and outputs a Markdown file for each item. 

# Dependencies

It was developed for python2 then converted to python3 and uses a few built in things such as sqlite3, datetime, hashlib and string Template. Additional libraries are:

- [feedparser](https://github.com/kurtmckee/feedparser) ([docs](https://pythonhosted.org/feedparser/))
- [yaml](https://pyyaml.org/) ([docs](https://pyyaml.org/wiki/PyYAMLDocumentation))

## Fedora/OpenBSD

	sudo dnf install python3-feedparser python3-yaml
	doas pkg_add py3-yaml py3-feedparser py3-dateutil

# Future Work aka TODOs

- Command line options
	- Replace all static paths with sane defaults and support command line arguments
	- Create functions to reduce needless execution i.e. Update.py: create tables; insert feed URLs: 
- Return values for use in scripts.
- Test Cases for seen issues:
	- Strange encoding
	- Duplicate postings
	- Posted, then removed, try not to add twice
- Auto create and backup data folder.

# Example Crontab

Hugo is installed via a snap (```snap install hugo```), normally a newer version that from the apt repositories. Due to the large number of pages (new items) the newer versions of hugo provide better performance. 

	SHELL=/bin/bash
	PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

	0 * * * * cd feed-backend/ && python update.py && python extract.py
	0 * * * * /snap/bin/hugo -d /home/user/static-generator/public/ -s /home/user/static-generator/ --log --logFile /home/user/static-generator/log 

	/usr/local/bin/hugo -d /var/www/htdocs/port22.co.uk -s /var/www/src/port22.co.uk 
