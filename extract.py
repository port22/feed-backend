#!/usr/bin/python

import sqlite3, datetime, sys, yaml
from string import Template
import time
from datetime import datetime
from dateutil import parser
from xml.sax.saxutils import escape

# TODO: CLI Parameter.
#outputDir = "./output" 
outputDir = "../../src/port22.co.uk/content"

frontMatter = Template(
'''
title: |-
    $title
author: $author
link: $link
date: $date
source_url: $source_url
source_name: $source_name

db_id: $db_id
db_createdAt: $date_added
''')

newsItem = Template(
'''
---
$frontMatter
---

$content
''')

opmlSubList = Template(
'''<?xml version="1.0" encoding="ISO-8859-1"?>
<opml version="2.0">
	<head>
		<title>port22_feeds.opml</title>
		<dateCreated>Mon, 07 Oct 2019 16:00:00 GMT</dateCreated>
		<dateModified>$date</dateModified>
		<ownerName>Pete Maynard</ownerName>
		<ownerEmail>feed@port22.co.uk</ownerEmail>
	</head>
	<body>
$outlineFeeds
	</body>
</opml>
''')
con = sqlite3.connect('data/data.db')
con.row_factory = sqlite3.Row
c = con.cursor()

# Create OPML subscription list for all feeds
feed_outlines = []
for r in c.execute('SELECT feed_title, feed_link FROM feed'):
	# TODO: Add these: description="" htmlUrl="" language="unknown" type="rss" version="RSS2"
        # TODO: Deal with disabled feeds
	feed_outlines.append('\t\t<outline text="{0}" title="{0}" xmlUrl="{1}"/>'.format(r[0], escape(r[1])))


with open(outputDir + '/port22_feeds.opml', 'w') as f:
	f.write(opmlSubList.safe_substitute(
		date = datetime.now(),
		outlineFeeds = '\n'.join([str(x) for x in feed_outlines])
	))

# Get all news items which have not been generated.
for row in c.execute('SELECT * FROM feed INNER JOIN news ON feed.fid = news.source_feed WHERE gen == 0'):
	outFile = outputDir + '/news/'+row['NID']+'.md'
	print("Writing - content/" + row['NID'] + '.md', end=' ')
	try:
		fm = frontMatter.substitute(
			title = row['title'], 
			link = row['link'], 
			author = row['author'].replace(':', "-"), 
			date = parser.parse(row['published'], ignoretz=True), 
			source_url = row['feed_link'],
			source_name = row['feed_title'],
			db_id = row['NID'], 
			date_added = row['date_added']
		)

		# Check for valid YAML
		yaml.safe_load(fm)
		
		with open(outFile, 'w') as f:
			f.write(newsItem.substitute(
			frontMatter = fm, 
			content = row['description']
		))
	
	except Exception as e: 
		print("Error, highly likely YAMLError:", row['title'], "NewsID:", row['NID'])
		print(e)
	else:
		# Let the database know which have been converted to markdown.
		try:
			con.execute('UPDATE news SET gen = 1 WHERE NID like ?', ([row['NID']]))
			con.commit()
		except Exception as e:
			print("Something went wrong updating the database.")
			print(e)
		else:
			print("[ok].")
